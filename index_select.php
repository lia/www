<!DOCTYPE html>
<html>
<head>
	<title>Index of Subdirectories - <?php echo basename(getcwd()); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style>
		:root {
			--primary-color: #1e88e5;
			--background-color: #f5f5f5;
			--text-color: #333;
			--box-shadow: 0 0 15px rgba(0, 0, 0, 0.1);
			--border-radius: 8px;
			--transition: 0.2s;
		}

		body {
			font-family: Arial, sans-serif;
			font-size: 16px;
			line-height: 1.5;
			padding: 20px;
			background-color: var(--background-color);
			color: var(--text-color);
		}

		h1 {
			margin-bottom: 20px;
			text-align: center;
			color: var(--primary-color);
		}

		ul {
			list-style: none;
			padding: 0;
			margin: 0;
			display: grid;
			grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
			gap: 20px;
			justify-items: center;
		}

		li {
			padding: 15px;
			background-color: #fff;
			box-shadow: var(--box-shadow);
			border-radius: var(--border-radius);
			display: flex;
			flex-direction: column;
			align-items: center;
			justify-content: center;
			transition: var(--transition);
		}

		li:hover {
			transform: translateY(-5px);
			box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
		}

		a {
			color: var(--primary-color);
			text-decoration: none;
			text-align: center;
		}

		a:hover {
			text-decoration: underline;
		}
	</style>
</head>
<body>
	<h1>Index of Subdirectories - <?php echo basename(getcwd()); ?></h1>
	<ul>
		<?php
			// Get a list of subdirectories in the current directory
			$subdirs = glob('*', GLOB_ONLYDIR);
			foreach ($subdirs as $subdir) {
				// Generate a link for each subdirectory
				echo '<li><a href="' . $subdir . '/">' . $subdir . '</a></li>';
			}
		?>
	</ul>
</body>
</html>



<?php
require_once '/eos/user/l/lia/www/Parsedown.php';
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo basename(getcwd()); ?></title>
    <style>
        body {
            font-family: Arial, sans-serif;
            line-height: 1.6;
        }

        h1 {
            text-align: center;
            margin-bottom: 30px;
        }

        .container {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
        }

        .plot {
            width: 30%;
            padding: 10px;
            box-sizing: border-box;
            text-align: center;
            margin-bottom: 20px;
        }

        img {
            width: 100%;
            height: auto;
            padding: 5px;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
            margin-bottom: 10px;
        }

        a {
            text-decoration: none;
            color: #333;
        }

        a:hover {
            text-decoration: underline;
        }

        .parent-btn {
            display: inline-block;
            padding: 10px 20px;
            background-color: #333;
            color: #fff;
            text-decoration: none;
            border-radius: 3px;
            margin: 10px;
        }

        .parent-btn:hover {
            background-color: #555;
        }

        .spacing {
            margin-bottom: 30px;
        }

        /* New class for subdirectory button styling */
        .subdir-btn {
            display: inline-block;
            padding: 8px 16px;
            background-color: #007bff;
            color: #fff;
            text-decoration: none;
            border-radius: 4px;
            margin: 5px;
            font-size: 14px;
            transition: background-color 0.3s;
        }

        .subdir-btn:hover {
            background-color: #0056b3;
        }

        .custom-text {
            text-align: left; /* Left-align the text */
            font-size: 18px;
            font-weight: normal; /* Set font weight to normal (not bold) */
            margin-bottom: 30px;
            color: #000000; /* Set the text color to black */
            background-color: #f8f9fa;
            padding: 20px;
            border: 3px dashed #007bff;
            border-radius: 10px;
            box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
        }
    </style>
</head>
<body>
    <h1><?php echo basename(getcwd()); ?></h1>
    <div class="spacing">
        <a href="../" class="parent-btn">Go to Parent Directory</a>
        <form method="GET">
            <label for="suffix">Filter: </label>
            <input type="text" id="suffix" name="suffix" value="<?php echo isset($_GET['suffix']) ? htmlspecialchars($_GET['suffix']) : ''; ?>">
            <input type="submit" value="Apply Filter">
        </form>
        <?php
        // Display subdirectories
        $subdirectories = glob('*/', GLOB_ONLYDIR);
        if (count($subdirectories) > 0) {
            echo '<div class="subdirectories">';
            echo '<h3>Subdirectories:</h3>';
            foreach ($subdirectories as $subdir) {
                echo '<a href="' . $subdir . '" class="subdir-btn">' . basename($subdir) . '</a>';
            }
            echo '</div>';
        }
        ?>
    </div>
    <?php
    $parsedown = new Parsedown();
    $markdownFile = 'README.md';

    if (file_exists($markdownFile)) {
        $markdownText = file_get_contents($markdownFile);
        $htmlText = $parsedown->text($markdownText);
    } else {
        $htmlText = '<p>README.md not found.</p>';
    }
    ?>
    <div class="custom-text">
        <?php echo $htmlText; ?>
    </div>
    <div class="container">
        <?php
        $dir = '.';
        $files = glob($dir . '/*.{png,pdf,eps}', GLOB_BRACE);
        sort($files);

        $displayedFiles = [];
        $suffix = isset($_GET['suffix']) ? $_GET['suffix'] : '';

        foreach ($files as $file) {
            $filename = pathinfo($file, PATHINFO_FILENAME);

            if ($suffix !== '' && strpos($filename, $suffix) === false) {
                continue;
            }

            if (in_array($filename, $displayedFiles)) {
                continue;
            }

            array_push($displayedFiles, $filename);

            $png = $filename . '.png';
            $pdf = $filename . '.pdf';
            $tex = $filename . '.tex';
            $eps = $filename . '.eps';

            echo '<div class="plot">';

            if (file_exists($png)) {
                echo '<img src="' . $png . '" alt="' . $filename . '">';
            } elseif (file_exists($pdf)) {
                echo '<object data="' . $pdf . '" type="application/pdf" width="100%" height="400px">';
                echo '  <p>It appears you don\'t have a PDF plugin for this browser.</p>';
                echo '  <p><a href="' . $pdf . '">Click here to download the PDF file.</a></p>';
                echo '</object>';
            } elseif (file_exists($eps)) {
                echo '<object data="' . $eps . '" type="application/postscript" width="100%" height="400px">';
                echo '  <p>It appears you don\'t have an EPS plugin for this browser.</p>';
                echo '  <p><a href="' . $eps . '">Click here to download the EPS file.</a></p>';
                echo '</object>';
            }

            echo '<p>';

            if (file_exists($png)) {
                echo '<a href="' . $png . '">PNG</a> | ';
            }
            if (file_exists($pdf)) {
                echo '<a href="' . $pdf . '">PDF</a>';
            }
            if (file_exists($tex)) {
                echo ' | <a href="' . $tex . '">TEX</a>';
            }
            if (file_exists($eps)) {
                echo ' | <a href="' . $eps . '">EPS</a>';
            }

            echo '</p>';
            echo '</div>';
        }
        ?>
    </div>
</body>
</html>

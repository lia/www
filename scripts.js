document.addEventListener("DOMContentLoaded", function () {
    const main = document.querySelector("main");

    const aboutSection = `
        <section id="about">
            <h1>Ang LI</h1>
            <img src="profile.jpeg" alt="Your profile picture" class="profile-picture">
            <p>Welcome to my personal website! As a dedicated PhD candidate, I am deeply passionate about High Energy Physics (HEP) and actively contribute to the ATLAS experiment and Future Circular Collider (FCC) projects at CERN. Currently based in Paris, I work at the AstroParticle and Cosmology Laboratory (APC) at Université Paris Cité and the National Centre for Scientific Research (CNRS). My primary research interests center around Higgs physics, where I endeavor to employ cutting-edge data analysis techniques to enhance our comprehension of the nature.</p>
        </section>
    `;

    const researchSection = `
        <section id="research">
            <h2>Research</h2>
            <p>At the ATLAS experiment, my work primarily involves calibrating b-tagging using the pT-Rel method, as well as conducting di-Higgs analyses on bbyy and bbtautau channels. Concurrently, for the Future Circular Collider (FCC) project, my research focuses on assessing the feasibility of precise Higgs mass determination and measuring the ZH cross-section. These efforts are essential for advancing our understanding of high-energy physics and furthering the development of groundbreaking discoveries in the field. Below are the subdirectories for specific research projects:</p>
            <div class="subdirectories">
            <a href="ATLAS" class="subdirectory-block">
            <h3>ATLAS</h3>
            <p>Click here to explore my work on the ATLAS project.</p>
            </a>
            <a href="FCC" class="subdirectory-block">
            <h3>FCC</h3>
            <p>Click here to explore my work on the FCC project.</p>
            </a>
            </div>
        </section>
    `;
    const publicationsSection = `
        <section id="publications">
            <h2>Publications</h2>
            <p>Here is a collection of my latest publications where I serve as the main contributor:</p>
            <ul>
                <li><a href="https://link.springer.com/content/pdf/10.1140/epjp/s13360-021-02202-4.pdf?pdf=button">A special Higgs challenge: measuring the mass and production cross section with ultimate precision at FCC-ee</a></li> 
                <li><a href="https://pos.sissa.it/380/420/pdf">Perspectives for Higgs measurements at Future Circular Collider</a></li>
                <li><a href="https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/HDBS-2018-34/">Search for Higgs boson pair production in the two bottom quarks plus two photons final state in pp collisions at √s=13 TeV with the ATLAS detector</a></li>
                <li><a href="https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2022-025/">Measurement of the b-jet identification efficiency with the pTrel method in multi-jet events using pp collisions at √s=13TeV with the ATLAS Detector</a></li>
                <!-- Add more publications as needed -->
            </ul>
        </section>
    `;

    const talksSection = `
        <section id="talks">
            <h2>Talks</h2>
            <p>Here is a collection of my talks:</p>
            <ul>
                <li><a href="https://indico.in2p3.fr/event/23012/contributions/89963/attachments/62029/84792/2021_01_21_FCC_Ang_LI_Ver2.pdf">HZ cross section measurement @ FCC-ee, 2nd FCC-France Workshop</a></li> 
                <li><a href="https://indico.lip.pt/event/592/contributions/3184/attachments/2678/4185/2021_09_05_FCC_PANIC_Ang_LI.pdf">Perspectives for Higgs measurements at Future Colliders, PANIC2021</a></li>
                <li><a href="https://indico.cern.ch/event/1030068/contributions/4513152/attachments/2330953/3972345/2021_10_20_FCC_HIGGS2021_Ang_LI.pdf">ZH and mH studies at FCC-ee, Higgs2021</a></li>
                <li><a href="https://indico.in2p3.fr/event/20459/contributions/102408/attachments/67190/94172/2021_11_23_FCC_IRN2021_Ang_LI.pdf">Higgs boson mass and production cross-section measurement at FCC-ee, IRN Terascale 2021</a></li>
                <li><a href="https://indico.in2p3.fr/event/22887/contributions/101476/attachments/67632/94881/2021_12_02_FCC_France_3rd_Ang_LI.pdf">The total e+e- → ZH cross section σHZ and mass measurement from the recoil, 3rd FCC-France / Higgs & ElectroWeak Factory Workshop, Annecy </a></li>
                <li><a href="https://indico.cern.ch/event/1066234/contributions/4708082/attachments/2385658/4077518/2021_02_07_FCCWEEK_Ang_LI.pdf">Higgs mass and ZH cross-section from Z(mumu)H events, 5th FCC Physics Workshop</a></li>
                <li><a href="https://indico.cern.ch/event/1176398/contributions/5208220/attachments/2582987/4455428/2023_01_27_FCC_Physics_Workshop_Ang_LI.pdf">Higgs mass and ZH cross-section, 6th FCC Physics Workshop</a></li>
                <li><a href="https://indico.cern.ch/event/1079757/contributions/4634084/attachments/2364940/4039351/2021_12_15_ATLAS_PP_week_Ang_LI.pdf">Comissioning of tracking and b-tagging in low pile-up, ATLAS Physics workshop 2021</a></li>
                <li><a href="https://indico.cern.ch/event/1149335/contributions/4924517/attachments/2511385/4316736/2022_09_20_pTRel_PAF_Ang_LI.pdf">b-tagging calibration with the pTrel method, Workshop Physics ATLAS France 2022</a></li>
                <!-- Add more publications as needed -->
            </ul>
        </section>
    `;
        
    const contactSection = `
        <section id="contact">
            <h2>Contact</h2>
            <p>If you would like to get in touch with me or discuss my research, please feel free to reach out:</p>
            <ul>
                <li><span>Email:</span>  <a href="mailto:ang.l@cern.ch">ang.l@cern.ch</a></li>
                <li><span>Gitlab:</span> <a href="https://gitlab.cern.ch/lia">gitlab.cern.ch/lia</a></li>
                <li><span>Github:</span> <a href="https://github.com/AngLICERN/">github.com/AngLICERN</a></li>
                <!-- Add more contact methods as needed -->
            </ul>
        </section>
    `;
 
    
    main.innerHTML = aboutSection + researchSection + publicationsSection + talksSection + contactSection;
});
